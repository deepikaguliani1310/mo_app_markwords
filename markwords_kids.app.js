/*global $, document, jQuery, jsApp, PORTFOLIO */
/*jslint browser: true */
/*portfolio require portfolio:jquery */
/*portfolio require portfolio:jquery.ui */
/*portfolio require common/html5shiv-printshiv.js */
/*portfolio require common/jsApp.extensions.js */
/*portfolio require common/jsApp.children.common.js */
/*portfolio require common/jsApp.messages.js */
/*portfolio require common/featherlight/featherlight.min.js */
/*portfolio require common/jsApp.messages.js */
/*portfolio require common/featherlight/featherlight.min.css */
/*portfolio require common/jsApp.children.common.css */
/*portfolio require markwords/markwords_kids.css */

var jsApp = jsApp || {};

jsApp.markwordsInit = function (appSettings) {

	'use strict';

	var app = jsApp.createChildrenAppBase(appSettings, "markwords"),
        words,
		headerString,
		numberOfWords,
        attempts,
		numberOfCorrectlySelectedWords,
		gameCompleted,
		$mainTask,
		$instructions,
		appPath,
		mode,
		successSound = 'success.mp3',
		failureSound = 'failure.mp3',

		onGameFinished = function () {
			gameCompleted = true;
			app.onGameFinish();
		},

		selectWord = function () {
			var $word = $(this);

			if (gameCompleted || $word.hasClass('markwords_selected')) {
				return;
			}

			if ($word.hasClass('markwords_correct')) {
				$word.addClass('markwords_selected');
				numberOfCorrectlySelectedWords += 1;
				app.playSound(appPath+successSound, 'markwords_success');
			} else {
				$word.addClass('markwords_flashRed');
				app.playSound(appPath+failureSound, 'markwords_failure');
				setTimeout(function () { $word.removeClass('markwords_flashRed'); }, 500);
			}

			$word.blur();

			attempts += 1;
			app.setPoints(numberOfCorrectlySelectedWords);

			if (numberOfCorrectlySelectedWords === numberOfWords) {
				onGameFinished();
			}
		},

		resetPlayingField = function () {
			var wordClass, wordPrefix, wordPostfix, $div;

			numberOfCorrectlySelectedWords = 0;
			attempts = 0;
			gameCompleted = false;
			app.resetPoints(numberOfWords)
			$mainTask.html('');

			$.each(words, function (i, word) {
				if (word.type === 'newline') {
					$mainTask.append('<br />');
				} else if (word.type === 'space') {
					$mainTask.append(' ');
				} else if (word.type === 'word') {
					$div = $('<div>');
					$div.append($('<span>').text(word.prefix));
					if (mode === "text") {
						$div.append($('<span>').text(word.value).addClass('markwords_word').addClass(word.cssClass).on('click', selectWord));
				    } else if (mode === "images") {
				    	var $image = $('<img/>',{ 'class' : 'markwords_image',
			    								  'width' : '100',
			    								  'height' : '100',
			    								   'src' : appPath + word.value
			    								});
				    	$div.append($('<span>').append($image).addClass('markwords_word').addClass(word.cssClass).on('click', selectWord));
				    }
					$div.append($('<span>').text(word.postfix));
					$mainTask.append($div);
				}
			});
			app.setLanguageForElements($mainTask, $instructions);
		},

		setElementShortcuts = function () {
			$mainTask = app.getAppClassElement('markwords_mainTask');
			$instructions = app.getTag('h2');
		},

        setElementWidths = function () {
        	//var elements = [{ element: $sidebar, percent: 25 }, { element: $mainTask, percent: 75}];
        	//app.setElementWidths(app.getAvailableWidth(), elements);
        },

		initPlayingField = function () {
			setElementShortcuts();
			app.initPlayingField(resetPlayingField, headerString);
			app.bindButtonClicksForAllSections();
			resetPlayingField();
			app.apply_i18n();
			$(window).resize(function () {
				//app.setFontSize();
				//setElementWidths();
			});
		},

		addCorrectWord = function (prefix, text, postfix) {
			words.push({ type: 'word', value: text, prefix: prefix, postfix: postfix, cssClass: 'markwords_correct' });
			numberOfWords += 1;
		},

		addIncorrectWord = function (prefix, text, postfix) {
			words.push({ type: 'word', value: text, prefix: prefix, postfix: postfix, cssClass: 'markwords_incorrect' });
		},

		addSpace = function () {
			words.push({ type: "space" });
		},

		addNewline = function () {
			words.push({ type: "newline" });
		},

		readContentAsyncComplete = function (contentText) {
			var parentUUID = $(contentText).find('object').attr('parent'),
				myPath;

            var customInit = $(contentText).find('object').attr('custom_init');

            var customInitHash = customInit ? app.getCustomInitHash(customInit, 'mark') : {};
            mode = customInitHash.mode ? customInitHash.mode : "text";
            if (customInitHash.hasOwnProperty("successSound")) {
                successSound = customInitHash.successSound;
            }

            if (customInitHash.hasOwnProperty("failureSound")) {
                failureSound = customInitHash.failureSound;
            }
            /*Now do the stuff that app.readContentAsync does, with the raw XML*/
            contentText = app.extractContent(contentText);

			var newLineIndex, wordPrefix, wordPostfix, hasNewLine, parts, partMatch;

			if (contentText.occurrences('\n') < 1) {
			    throw new Error(app.gettext('The content must consist of at least two lines; a header and an assigment (which may span multiple lines)'));
			}

			newLineIndex = contentText.indexOf('\n');
			headerString = contentText.slice(0, newLineIndex);
			contentText = contentText.slice(newLineIndex + 1).trim();

			if (contentText.length < 1) {
			    throw new Error(app.gettext('The content text cannot be empty'));
			}

            /* replace spaces inside [] , with keyword to detect */
            if( contentText.match(/\[(.*?)\]/g) ){
                var matchParts = contentText.match(/\[(.*?)\]/g);
                $.each( matchParts , function( i ,matchPart ){
                       contentText = contentText.replace(matchPart ,matchPart.replace(/\s/g,'{space}'));
                });
            }

			contentText = contentText.replace(/\n/g, '{{newline}}\n');
			parts = contentText.split(/[ \n]/);
			words = [];
			numberOfWords = 0;

			$.each(parts, function (i, part) {
				hasNewLine = part.contains('{{newline}}');
				part = part.replace(/\{\{newline\}\}/g, '');
				wordPrefix = part.match(app.wordPrefixRegex);
				wordPrefix = wordPrefix === null ? '' : wordPrefix[0];
				part = part.replace(app.wordPrefixRegex, '');
				wordPostfix = part.match(app.wordPostfixRegex);
				wordPostfix = wordPostfix === null ? '' : wordPostfix[0];
				part = part.replace(app.wordPostfixRegex, '');

                /* Changed the logic for Markwords for making multiple
                words work within [] */

                /*  to make "#(xxx)"  work like #"(xxx)" */
                /* to see what prefix needs to be added since that
                would not be added earlier */
                /* since the last part of is taken care of ,
                we need to handle on the prefix */

                /* Changes made for markwords prefix issue ,
                include special characters in selected words */

                if( part.match(/[\.,\?!;:"'\(]/g) &&
                  ( part.contains('#')  || ( part.contains('[') && part.contains(']') ))){
                    if( part.match(/[\.,\?!;:"'\(]/g) && wordPrefix=='' ){
                        var partReplica = part;
                        partReplica = partReplica.replace(/[#\[]/g,'');
                        var replicaMatch = partReplica.match(/^[\.,\?!;:"'\(]/g);
                        wordPrefix = replicaMatch === null ? '' : replicaMatch[0];
                        part = part.replace(wordPrefix,'');
                    }
                }

				/* if it is a marked word , add correct word ,
                which may have # or [ */

				if (part.charAt(0) === '#' || part.charAt(0) === '[') {

                    /* replace the keywords with actual spaces for
                    multi words */
                    if( part.contains('{space}') ){
                            part = part.replace(/\{space\}/g,' ');
                    }

                    var charAtZero = part.charAt(0);
                    var indexOfHash = part.indexOf('#');
                    var indexOfStartBracket = part.indexOf('[');
                    var indexOfEndBracket = part.indexOf(']');
                    var partLength = part.length;

                    /* Calculate part string on the basis of
                    first character */
                    if( charAtZero === '#' ){
                        if( (indexOfStartBracket !=-1) && (indexOfEndBracket !=-1) ){
                            part = part.substr( (indexOfStartBracket+1) , indexOfEndBracket-2 ).trim();
                        }
                        else{
                            part = part.substr( (part.indexOf(charAtZero)+1) , (partLength) ).trim();
                        }
                    }
                    else if( charAtZero === '[' ){
                        part = part.substr( (indexOfStartBracket+1) , (indexOfEndBracket-1) ).trim();
                    }
					addCorrectWord(wordPrefix, part, wordPostfix);
				} else {
					addIncorrectWord(wordPrefix, part, wordPostfix);
				}
				addSpace();
				if (hasNewLine) { addNewline(); }
			});

			if (numberOfWords < 1) {
			    throw new Error(app.gettext('The content text must contain at least one test word/phrase'));
			}

			myPath = app.extractPath(appSettings.app_base_url);
			appPath = myPath + parentUUID +'/';

			$(appSettings.dom_id).find('div.js-app-container').load(appSettings.app_base_url + 'markwords/markwords_kids.htm', initPlayingField);
		};

	app.showStartScreen('Markwords', readContentAsyncComplete, true);
};

$(document).ready(function () {
    var apps = PORTFOLIO.APP.get('markwords_kids');
    $.each(apps, function (i, appSettings) {
        jsApp.markwordsInit(appSettings);
    });
    return true;
});