/*global $, document, jQuery, window, console */

var jsApp = jsApp || {};

jsApp.createChildrenAppBase = function(settings, classPrefix) {
	'use strict';

    var rtlLanguageCodes = ['ar-iq', 'ar-ma', 'fa-ir', 'ku-ir', 'ur-pk'],
		vowels = 'aeiouyæøåAEIOUYÆØÅéÉëËèÈêÊäÄáÁâÂàÀìÌíÍïÏîÎóÓòÒôÔöÖùÙûÛÿ',
		contentLanguageCode,
		rtlLanguage,
		resetGameFunction,
		cancelResetGameFunction,
		resetConfirmationDisabled,
		currentRecord,
		$confirmResetDialog,
		$correctIndicator,
		$correctIndicatorText,
		$progressIndicator,
		$progressIndicatorText,
		$recordText,
        /** for drag and drop dailog box */
        $dragAndDropDailog,
        $pointsIndicatorContainer,
        $leaveButton,
        $stayButton,
        $gameContainer,
        $leaveContainer,
        $gameFinishContainer,
        $playAgainButton,
        $gameCloseButton,
        $dragAndDropSection,

        /**
         * Prints a debug message to the console (if the console is defined).
         * @param {String} text Message to print.
         */
		debug = function (text) {
		    if (window.console !== undefined) {
		        console.log(text);
		    }
		},

        /**
         * Gets a translated version of the specified string.
         * @param {String} str String to translate.
         * @returns {String} Translated string, or the same string if no translation exists.
         */
		gettext = function (str) {
		    // Client language not set
		    if (!settings.ui_language) { return str; }

		    // Lookup table must exist
		    if (!jsApp.messages) { return str; }

		    // Original string must exist
		    if (!jsApp.messages[str]) { return str; }

		    // Translation must exist
		    if (!jsApp.messages[str][settings.ui_language]) { return str; }

		    // Everything is ok, return translated string
		    return jsApp.messages[str][settings.ui_language];
		},

        /**
         * Parses the specified CSS pixel value for the specified jQuery object.
         * @param {jQuery} $element jQuery object from which to read the value.
         * @param {String} value CSS value to parse.
         * @returns {int} Parsed CSS value.
         */
		parseCSSValue = function ($element, value) {
            return parseInt($element.css(value).replace('px', ''), 10);
        },

	    //
        //
		/**
         * Selects one or more element(s) below the dom_id.
         * Tries first with the app specific classPrefix (e.g. '.cat_', then with the general prefix ('.js-app').
         * @param {String} className Class name to search for.
         * @returns {jQuery} A jQuery object.
         */
		getClass = function (className) {
		    var $element = $(settings.dom_id + ' .' + classPrefix + className);
		    if ($element.length > 0) {
		        return $element;
		    }

		    if (!className) {
		        className = '';
		    } else if (className.length > 0 && className.charAt(0) !== '-') {
		        className = '-' + className;
		    }

		    return $(settings.dom_id + ' .js-app' + className);
		},



        /*
         * System can choose to run tapNtap on a non-touch screen.
         */
        overrideDragNDrop = function() {
            var accessibility = readNamedConfigVariable('accessibility'), parsed;

            if (accessibility =='') {
                return false;
            }

            accessibility = accessibility.replace(/\'/g,"\"");
            accessibility = accessibility.replace(/-/g,"\_");

            parsed = $.parseJSON(accessibility);
            return (parsed.drag_and_drop == 0);

        },

        /**
         * Sets the font size according to the app width, by adding (and removing) CSS classes to the app root element.
         */
		setFontSize = function () {
            var $app = getClass(), appWidth = $app.width();
            if (appWidth >= 620) {
                $app.addClass('js-app-XL').removeClass('js-app-L js-app-M js-app-S js-app-XS');
            } else if (appWidth > 560) {
                $app.addClass('js-app-L').removeClass('js-app-XL js-app-M js-app-S js-app-XS');
            } else if (appWidth > 510) {
                $app.addClass('js-app-M').removeClass('js-app-XL js-app-L js-app-S js-app-XS');
            } else if (appWidth > 450) {
                $app.addClass('js-app-S').removeClass('js-app-XL js-app-L js-app-M js-app-XS');
            } else {
                $app.addClass('js-app-XS').removeClass('js-app-XL js-app-L js-app-M js-app-S');
            }
        },

        /**
         * Tries to read a named configuration variable, but unlike readNamedConfigVariable only
         * for the users account, not site or global settings.
         */
        readNamedConfigVariableOnlyAccount = function (variableName) {
            var accountURL, result;
            accountURL = settings.site_url + 'xml/read_config/account/'+variableName;
            result = synchronousLoad(accountURL);

            if ($(result).find('config').length > 0 ) {
                return $(result).find('config').text();
            }
            return "";

        },

        /**
         * Reads a named config variable, first trying account spesific vars, then site, then global.
         */
        readNamedConfigVariable = function (variableName) {
            var accountURL, siteURL, globalURL, result;

            accountURL = settings.site_url + 'xml/read_config/account/'+variableName;
            siteURL = settings.site_url + 'xml/read_config/site/'+variableName;
            globalURL = settings.site_url + 'xml/read_config/global/'+variableName;

            result = synchronousLoad(accountURL);

            if ($(result).find('config').length > 0 ) {
                return $(result).find('config').text();
            }

            result = synchronousLoad(siteURL);

            if ($(result).find('config').length > 0 ) {
                return $(result).find('config').text();
            }

            result = synchronousLoad(globalURL);

            if ($(result).find('config').length > 0 ) {
                return $(result).find('config').text();
            }
            return "";
        },

        /**
         * Helper method for synchronous load.
         */

        synchronousLoad = function (url) {
            var myResult;
            jQuery.ajax({
                url:    url ,
                success: function(result) {
                    myResult = result;
                    },
                async:   false
            });
            return myResult;
        },

        /**
         * Gets all HTML tags of the specified type below the dom_id.
         * @param {String} tagName The tag name to search for.
         * @returns {jQuery} A jQuery object.
         */
		getTag = function (tagName) {
            return $(settings.dom_id + ' ' + tagName);
        },

		/**
         * Gets the settings URL for the app input ID.
         * @returns {String} The settings URL.
         */
		getContentUrl = function () {
		    return settings.site_url + 'xml/read/' + settings.app_input_id;
		},

		/**
         * Gets the read record URL for the current main object ID.
         * @returns {String} The read record URL.
         */
		getReadRecordUrl = function () {
		    return settings.site_url + 'xml/list_log?action=object_score;object=' + settings.main_object_id + ';response=xml';
		},

		/**
         * Gets the post record URL for the current main object ID.
         * @returns {String} The post record URL.
         */
		getPostRecordUrl = function () {
		    return settings.site_url + 'xml/score';
		},

		/**
         * Adds a 'new record' message to the $recordText element, as well as a class to highlight the message.
         */
		showNewRecordMessage = function () {
		    $recordText.addClass('js-app-newRecord').text(gettext('New record') + '!');
		},

		/**
         * Adds an 'equaled record' message to the $recordText element, as well as a class to highlight the message.
         */
		showEqualedRecordMessage = function () {
		    $recordText.addClass('js-app-newRecord').text(gettext('Match') + '!');
		},

		/**
         * Adds a 'record not set' message to the $recordText element.
         */
		showRecordNotSetMessage = function () {
		    $recordText.text(gettext('Not set'));
		},

		/**
         * Shows the current record in the $recordText element.
         */
		showCurrentRecord = function () {
		    if (currentRecord !== undefined && currentRecord >= 0) {
		        $recordText.removeClass('js-app-newRecord').text(currentRecord + gettext('%'));
		    }
		},

		/**
         * Displays the records div by fading it in.
         */
		displayRecordsDiv = function () {
		    getClass('records').fadeIn(2000);
		},

		/**
         * Posts a new record to the server (asynchronously). Any errors are ignored.
         * @param {int} record. The new record.
         */
		postNewRecord = function (record) {
		    var url = getPostRecordUrl();
		    $.ajax({
		        type: 'POST',
		        url: url,
		        data: 'object=' + settings.main_object_id + ';score=' + record
		    });
		},

		/**
         * Displays a confirm dialog, and then resets the board using the resetGameFunction (if defined, it must be set in advance).
         * The actual confirm dialog is created the first time this function is called, and then re-used on subsequent calls.
         * This function is typically called from an onClick event.
         */
		reset = function () {
		    if (resetConfirmationDisabled && resetGameFunction) {
		        resetGameFunction();
		        return;
		    }

		    if (!$confirmResetDialog) {
		        $confirmResetDialog = getClass('confirmResetDialog').dialog({
		            autoOpen: false,
		            resizable: false,
		            height: 160,
		            modal: true,
		            show: 'fade',
		            hide: 'fade',
		            title: gettext('Start over?'),
		            dialogClass: 'js-app-confirmResetDialog',
		            open: function (event, ui) {
		                $(this).closest('.ui-dialog').find('.ui-dialog-titlebar-close').hide();
		                $(this).siblings('.ui-dialog-buttonpane').find('button:eq(1)').focus();
		            }
		        });
		        $confirmResetDialog.dialog('option', 'buttons', [
					{ text: gettext('Yes'), click: function () { if (resetGameFunction) { resetGameFunction(); } $(this).dialog('close'); } },
					{ text: gettext('No'), click: function () { if (cancelResetGameFunction) { cancelResetGameFunction(); } $(this).dialog('close'); } }]);
		    }
		    $confirmResetDialog.dialog('open');
		},

		/**
         * Replaces HTML special characters with the respective HTML codes.
         * @param {String} str String to process.
         * @returns {String} HTML safe string.
         */
		safeString = function (str) {
		    return str.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
		},

		/**
         * Displays an error screen inside the dom_id. Typically used when an unrecoverable error has occurred.
         * @param {String} errorMessage The error message to display to the user.
         * @param {String} errorDetails Details about the error.
         */
		showErrorScreen = function (errorMessage, errorDetails) {
		    var $innerDiv, $outerDiv, $header, $content, $footer, $closeButton, $errorImage, self = this;
		    $outerDiv = $('<section>').addClass('js-app-error');
		    $header = $('<header/>');
		    $errorImage = $('<img/>',{'class' : 'error-avatar', 'src' : settings.app_base_url + 'common/images/robo_med.png'});
		    $header.append($errorImage);
		    $content = $('<div/>').addClass('error-content');
		    $content.append($('<h3/>',{'class' : 'error-oops'}).text(gettext('Oops!')));
		    $content.append($('<h3/>',{'class' : 'error-message'}).text(gettext('Something went wrong.') + errorDetails));
		    $footer = $('<footer/>').addClass('error-footer');
		    $footer.append($('<button/>').addClass('js-app-button-big try-again').text('Try Again'));
		    $closeButton = $('<button/>').addClass('js-app-button-big close-again').text('Close');
		    $closeButton.on('click', function() { $(settings.dom_id).find('.featherlight').remove(); })
		    $footer.append($closeButton);
		    $outerDiv.append($header).append($content).append($footer);
		    $(settings.dom_id).find('.js-app-container').html($outerDiv);

		},

		/**
         * Sets the contentLanguageCode and the rtlLanguage flag based on the information found in the specified content element.
         * @param {jQuery} $contentElement The content element to parse.
         */
		parseContentLanguage = function ($contentElement) {
		    var language;
		    if ($contentElement !== undefined) {
		        language = $contentElement.attr('language');
		        if (language !== undefined) {
		            contentLanguageCode = language.toLowerCase().replace('_', '-');
		            if (contentLanguageCode.length >= 2) {
		                rtlLanguage = $.inArray(contentLanguageCode, rtlLanguageCodes) > -1;
		            }
		        }
		    }
		},

		/**
         * Informs the user that the content format is invalid by showing an error screen.
         * @param {String} errorDetails Details about the error.         *
         */
		showContentErrorScreen = function (errorDetails) {
		    showErrorScreen(gettext('Invalid content format.'), errorDetails);
		    if (errorDetails !== undefined) {
		        debug('Content error: ' + errorDetails);
		    }

		};

	// Make sure that the dom_id starts with a '#'.
	if (settings.dom_id.charAt(0) !== '#') {
		settings.dom_id = '#' + settings.dom_id;
	}

	// Make sure that the site_url ends with a '/'.
	if (settings.site_url.length > 0 && settings.site_url.charAt(settings.site_url.length - 1) !== '/') {
	    settings.site_url += '/';
	}

	// Return an object that each app can interact with.
	return {
		settings: settings,
		getClass: getClass,
        getTag: getTag,
        gettext: gettext,
        setFontSize: setFontSize,
        parseCSSValue: parseCSSValue,
        isRtl: function () { return rtlLanguage; },
		wordPrefixRegex: /^[\.,\?!;:"'\(]+/,
		wordPostfixRegex: /[\.,\?!;:"'\)]+$/,

	    /**
         * Creates a new GUID.
         * @returns {String} A new GUID.
         */
        createGuid: function () {
            var S4 = function () {
                return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
            };
            return (S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4())
        },

        apply_i18n : function () {
        	var self = this;
            $.each($(settings.dom_id).find('.i18n'), function (i, el) {
                var content = $(el).text();
                if (content!== undefined) {
                    $(el).text(self.gettext(content));
                }
            });
        },

	    /**
         * Distributes the specified available width among the specified elements.
         * @params {int} availableWidth The available width.
         * @params {Array} elements List of elements.
         * @params {jQuery} elements.element jQuery object representing a single element.
         * @params {int} elements.percent Fraction (in percent, 0-100) of the available width that should be allocated to this element.
         */
        setElementWidths: function (availableWidth, elements) {
		    var margins = 0, borders = 0, padding = 0, remainingWidth, width, inaccuracyFix = 2;

		    if (availableWidth === undefined || isNaN(availableWidth) || elements === undefined || !(elements instanceof Array)) {
		        return;
		    }

		    $.each(elements, function (i, element) {
		        if (!element.element || !(element.element instanceof jQuery)) { return true; }
		        margins += parseCSSValue(element.element, 'margin-left') + parseCSSValue(element.element, 'margin-right');
		        padding += parseCSSValue(element.element, 'padding-left') + parseCSSValue(element.element, 'padding-right');
		        borders += parseCSSValue(element.element, 'border-left-width') + parseCSSValue(element.element, 'border-right-width');
		    });
		    availableWidth -= margins + padding + borders;
		    remainingWidth = availableWidth;

		    $.each(elements, function (i, element) {
		        if (!element.percent || isNaN(element.percent) || element.percent < 0 || element.percent > 100) {
		            element.percent = 100 / elements.length;
		        }
		        if (remainingWidth <= 0) {
		            element.element.hide();
		        } else {
		            width = Math.min(Math.floor((element.percent / 100) * availableWidth), (remainingWidth - inaccuracyFix));
		            element.element.width(width);
		            element.element.show();
		            remainingWidth -= width;
		        }
		    });

		    if (remainingWidth > 0) {
		        elements[elements.length - 1].element.width(elements[elements.length - 1].element.width() + (remainingWidth - inaccuracyFix));
		    }
		},

		getAppClassElement : function (className) {
		    var $element = $(settings.dom_id + ' .' + className);
		    if ($element.length > 0) {
		        return $element;
		    }

		    if (!className) {
		        className = '';
		    } else if (className.length > 0 && className.charAt(0) !== '-') {
		        className = '-' + className;
		    }

		    return $(settings.dom_id + className);
		},

		closeAppLayer : function() {
			if (typeof soundManager !== "undefined") {
				soundManager.stopAll();
			}
        	$(settings.dom_id).find('.featherlight').remove();
        },

		/* extracts path */
		extractPath : function (baseURL) {
            var parts, baseurl, i;
            baseurl = "";
            parts = baseURL.split('/');
            parts.pop();
            parts.pop();
            for (i = 0 ; i < parts.length; i+=1) {
                baseurl += parts[i] + '/';
            }
            return baseurl;
        },

	    /**
         * Determines if the specified letter is a vowel.
         * @param {String} letter Single character string.
         * @returns {Boolean} True if the letter is a vowel, otherwise false.
         */
        isVowel: function (letter) {
			return vowels.indexOf(letter) > -1;
		},

	    /**
         * Sets the 'lang' attribute of the specified arguments (elements) to the contentLanguageCode (if defined).
         * Also sets the 'dir' attributes to 'rtl' if the language should be rendered right-to-left, as well
         * as the CSS value 'text-align' to 'right'.
         */
        setLanguageForElements: function () {
			$.each(arguments, function (i, $element) {
				if (contentLanguageCode !== undefined) {
				    $element.attr('lang', contentLanguageCode);
				}
				if (rtlLanguage) {
				    $element.attr('dir', 'rtl').css('text-align', 'right');
				}
			});
		},

		showCustomChildError: function () {
			var $innerDiv, $outerDiv, $header, $content, $footer;
		    $outerDiv = $('<section>').addClass('error');
		    $header = $('<header/>').addClass('error-header').append($('<h3/>').text("Something went terribly wrong"));
		    $content = $('<div/>').addClass('error-content');
		    $footer = $('<footer/>').addClass('js-app-footer error-footer');
		    $footer.append($('<button/>').addClass('js-app-button-big try-again').text('Try Again'));
		    $footer.append($('<button/>').addClass('js-app-button-big close-again').text('Close'));
		    $outerDiv.append($header).append($content).append($footer);
		    $(settings.dom_id).find('.js-app-container').html($outerDiv);
		},

	    /**
         * Enable the reset button
         */
        enableResetButton: function () {
			getClass('resetButton').removeAttr("disabled");
		},

	    /**
         * Disable the reset button
         */
        disableResetButton: function () {
			getClass('resetButton').attr('disabled', 'disabled');
		},

	    /**
         * Returns the available width inside the element with the specified class name.
         * @param {String} className Class name of element to calculate available width for. Use empty string to get the width of the app's main area.
         */
        getAvailableWidth: function (className) {
		    var $div = this.getAppClassElement(className),
				innerWidth = $div.innerWidth(),
				paddingLeft = parseInt($div.css('padding-left'), 10),
				paddingRight = parseInt($div.css('padding-right'), 10);
			return innerWidth - paddingLeft - paddingRight;
		},

	    /**
         * Initializes the playing field.
         * @param {function} resetFunc Function called after the reset button is clicked and the user confirms the reset.
         * @param {jQuery} headerText Text to put in the header element.
         * @param {function} cancelResetFunc Function called after the reset button is clicked and the user does not confirm the reset.
         */
        initPlayingField: function (resetFunc, headerText, cancelResetFunc) {
			var $gettextCandidates = getClass('gettext');
			$gettextCandidates.each(function (i, element) { $(element).text(gettext($(element).text())); });
			$correctIndicator = getClass('correctIndicator');
			$progressIndicator = getClass('progressIndicator');
			$progressIndicatorText = getClass('progressBarText');
			$correctIndicatorText = getClass('correctBarText');
			getClass('resetButton').on('click', reset);
            /** Add css class for instructions (headers) **/
			this.getAppClassElement('js-app-instruction').html(headerText);
			resetGameFunction = resetFunc;
			cancelResetGameFunction = cancelResetFunc;
			//setFontSize();

			$pointsIndicatorContainer = getClass('pointsIndicatorContainer');
			$gameContainer = this.getAppClassElement('featherlight-content').find('.container');
			$leaveContainer = this.getAppClassElement('featherlight-content').find('.leave');
			$gameFinishContainer = this.getAppClassElement('featherlight-content').find('.finish');
			$playAgainButton = $gameFinishContainer.find('.play-again');
			$gameCloseButton = $gameFinishContainer.find('.close-game');
			$dragAndDropSection = this.getAppClassElement('featherlight-content').find('.drag-and-drop');
		},

	    /**
         * Sets both progress bars to the empty state (0%). A class is added which prevents the border around the progress bars to show.
         * @param {int} numberOfAssignments The total number of assigments in the current game.
         */
        resetProgressBars: function (numberOfAssignments) {
			$correctIndicator.width('0%');
			$progressIndicator.width('0%');
			$correctIndicator.addClass('js-app-progress_empty');
			$progressIndicator.addClass('js-app-progress_empty');
			$progressIndicatorText.html('0 ' + gettext('of') + ' ' + numberOfAssignments);
			$correctIndicatorText.html('0' + gettext('%'));

			this.disableResetButton();
			showCurrentRecord();
		},

		onGameFinish: function () {
			$gameContainer.hide("slide", { direction: "down"}, function(){
				$gameFinishContainer.show("slide", { direction: "up" });
			});
		},

        /**
         * Sets the number of earned points to empty state - 0.
         * @param {int} numberOfAssignments The total number of assigments in the current game.
         */

        resetPoints: function (numberOfAssignments) {
        	$pointsIndicatorContainer.empty();
			for (var i=0; i<numberOfAssignments; i++) {
				var $point = $('<span/>',{'class' : 'point not-solved'});
				if (numberOfAssignments > 20) {
					$point.addClass('point-medium');
				}
				$pointsIndicatorContainer.append($point);
			}
		},

         /**
         * Sets the progress bars.
         * @param {int} progress The number of completed assignments.
         * @param {int} attempts The number of solve attempts.
         * @param {int} numberOfAssignments The number of assignments in the current game.
         * @param {int} numberOfCorrectlySolvedAssignments The number of completed and correctly solved assignments. Only neccesary if progress is not equal to correctly solved assignments. If undefined, progress is used instead.
         * edit: if numberOfCorrectlySolvedAssignments is 0, correctPercentage is 0 also; in previous case that would return 100% which is incorrect
         */
        setProgressBars: function (progress, attempts, numberOfAssignments, numberOfCorrectlySolvedAssignments) {
			var solvedPercentage = Math.round(100 * (progress / numberOfAssignments)),
				//correctPercentage = Math.round(100 * ((numberOfCorrectlySolvedAssignments || progress) / attempts));
				// Lucas edit 18.11.2014
				correctPercentage = numberOfCorrectlySolvedAssignments ?  Math.round(100 * (numberOfCorrectlySolvedAssignments / attempts)) : 0;

			if (solvedPercentage > 0) {
				$correctIndicator.removeClass('js-app-progress_empty');
				$progressIndicator.removeClass('js-app-progress_empty');
			}

			$progressIndicatorText.text(progress + ' ' + gettext('of') + ' ' + numberOfAssignments);
			$progressIndicator.animate({ width: solvedPercentage + '%' }, 500);

			$correctIndicatorText.text(correctPercentage + gettext('%'));
			$correctIndicator.animate({ width: correctPercentage + '%' }, 500);

			$correctIndicator.removeClass('js-app-progress_green js-app-progress_yellow js-app-progress_red');

			if (correctPercentage >= 80) {
				$correctIndicator.addClass('js-app-progress_green');
			} else if (correctPercentage >= 50) {
				$correctIndicator.addClass('js-app-progress_yellow');
			} else {
				$correctIndicator.addClass('js-app-progress_red');
			}

			this.enableResetButton();
			resetConfirmationDisabled = false;
		},

		/**
         * Sets the points- new progess indicator - only the .
         * @param {int} progress The number of completed assignments.
         * @param {int} attempts The number of solve attempts.
         * @param {int} numberOfAssignments The number of assignments in the current game.
         * @param {int} numberOfCorrectlySolvedAssignments The number of completed and correctly solved assignments. Only neccesary if progress is not equal to correctly solved assignments. If undefined, progress is used instead.
         * edit: if numberOfCorrectlySolvedAssignments is 0, correctPercentage is 0 also; in previous case that would return 100% which is incorrect
         */

        setPoints : function(numberOfCorrectlySolvedAssignments) {
        	var points = $pointsIndicatorContainer.children();
        	for (var i=0; i<numberOfCorrectlySolvedAssignments; i++) {
        		$(points[i]).removeClass('not-solved').addClass('solved');
        	}
        },

        /**
        * For some use cases we only want the progress bar, not the % correct bar
        * @param {int} progress The nunber of completed assignments
        * @param {int} numberOfAssignments The total number of assignments.
        */
        setProgressBarOnlyProgress : function (progress, numberOfAssignments) {
            var solvedPercentage = Math.round(100 * (progress / numberOfAssignments));

            if (progress >0 ) {
                $progressIndicator.removeClass('js-app-progress_empty');
            }

            $progressIndicatorText.text(progress + ' ' + gettext('of') + ' ' + numberOfAssignments);
			$progressIndicator.animate({ width: solvedPercentage + '%' }, 500);

            this.enableResetButton();
            resetConfirmationDisabled = false;
        },

	    /**
         * Register the score after a completed game.
         * @param {int} score The score.
         */
        registerScore: function (score) {
			if (currentRecord !== undefined) {
				if (currentRecord === -1) {
					postNewRecord(score);
					currentRecord = score;
					showCurrentRecord();
				} else if (score > currentRecord) {
					postNewRecord(score);
					currentRecord = score;
					showNewRecordMessage();
				} else if (score === currentRecord) {
					showEqualedRecordMessage();
				}
			}

			resetConfirmationDisabled = true;
		},

	    /**
         * Displays a load screen inside the dom_id.
         */
        showLoadScreen: function () {
		    var $innerDiv, $outerDiv;
		    $outerDiv = $('<article>').addClass('js-app-children js-app-children-border');
		    $outerDiv.attr('id','js-app-memory');
		    $innerDiv = $('<div>').addClass('js-app-loadScreen');
		    $innerDiv.append($('<img>').addClass('js-app-loadImage').attr('src', settings.app_base_url + 'common/ajax-loader.gif'));
		    $innerDiv.append($('<p>').text(gettext('Loading task...')));
		    $outerDiv.append($innerDiv);
		    $(settings.dom_id).html($outerDiv);
		},

		showLoadingInLayer: function() {
			$(settings.dom_id).find('.js-app-container').empty().append($('<div/>',{'class' : 'loading-wrapper'})
																		.append($('<h3/>',{'class' : 'loading'}).text('Loading...')));
		},

		getCustomInitHash: function(custom_init, app) {
			var customInitHash = {};
            /* getting the custom parameters for speed */
            if (custom_init.indexOf('|') >= 0) {
                var values = custom_init.split('|');
                /* get rid of the app parameter */
                values.shift();
                if (values.length > 0) {
	                var params = values;
	                $.each(params , function( i, param ){
	                	var paramSplit = param.split("="),
	                    	key = (paramSplit[0]).replace(app+"_",""),
	                    	value = paramSplit[1];
	                    customInitHash[key] = value;
	                });
            	}
            }
            return customInitHash;
		},

		bindButtonClicksForAllSections: function() {
			var self = this,
				$gameContainer = self.getAppClassElement('featherlight-content').find('.container'),
				$leaveContainer = self.getAppClassElement('featherlight-content').find('.leave'),
				$smallScreenContainer = self.getAppClassElement('featherlight-content').find('.small-screen'),
				$dragAndDropSection = self.getAppClassElement('featherlight-content').find('.drag-and-drop'),
				$playAgainButton = self.getAppClassElement('featherlight-content').find('.play-again'),
				$gameCloseButton = self.getAppClassElement('featherlight-content').find('.close-game')
			/* Bind clicks for leave game section */
			$leaveContainer.find('.stay').on('click', function() {
				$gameContainer.show();
				$leaveContainer.hide();
			});
			$leaveContainer.find('.leave-game').on('click', function() {
				self.closeAppLayer();
			});

			/*Bind button clicks for small screen Message */
			$smallScreenContainer.find('.go-back').on('click', function() {
				self.closeAppLayer();
			});

			/*Bind Drag and Drop Warning Button Clicks */
			$dragAndDropSection.find('.ok').on('click', function() {
				$gameContainer.show();
				$dragAndDropSection.hide();
			});

			/*Bind game Finish Button Clicks */
			$playAgainButton.click(function(resetCallBack){
                $gameFinishContainer.hide();
                $gameContainer.show();
                //self.updateAriaProperties();
                if (typeof resetGameFunction === "function") {
                	resetGameFunction();
                }
            });

            $gameCloseButton.click(function() {
                self.closeAppLayer();
                //self.updateAriaProperties();
            });

		},

		/**
		  * Displays start page
		  */
		 showStartScreen: function(gameName, readContentCallback, isRaw) {
		 	var self = this;
		 	var $innerDiv, $container, $play;
		    $innerDiv = $('<div/>').addClass('js-app-startScreen js-app-children-border');
		    $container = $('<div/>').addClass('js-app-container').attr('id','js-app-'+classPrefix);
		    $innerDiv.append($('<h3/>',{'class' : 'start-app-name'}).text(gettext(gameName)));
		    $play = $('<button/>',{'class' : 'js-app-button-big uppercase'}).text(gettext('Play'));
		    $play.attr('data-featherlight','#js-app-'+classPrefix);
		    $play.attr('data-featherlight-variant','js-app-layer');
		    $innerDiv.append($play);
		    $(settings.dom_id).append($innerDiv).append($container);
		    $play.featherlight($container, {
				namespace:    'featherlight',         /* Name of the events and css class prefix */
				targetAttr:   'data-featherlight',    /* Attribute of the triggered element that contains the selector to the lightbox content */
				variant:      'js-app-layer',         /* Class that will be added to change look of the lightbox */
				resetCss:     false,                  /* Reset all css */
				background:   null,                   /* Custom DOM for the background, wrapper and the closebutton */
				openTrigger:  'click',                /* Event that triggers the lightbox */
				closeTrigger: 'click',                /* Event that triggers the closing of the lightbox */
				filter:       null,                   /* Selector to filter events. Think $(...).on('click', filter, eventHandler) */
				root:         settings.dom_id,         /* Where to append featherlights */
				openSpeed:    250,                    /* Duration of opening animation */
				closeSpeed:   250,                    /* Duration of closing animation */
				closeOnClick: false,           /* Close lightbox on click ('background', 'anywhere', or false) */
				closeOnEsc:   false,                  /* Close lightbox when pressing esc */
				closeIcon:    '',             /* Close icon */
				loading:      'loading',              /* Content to show while initial content is loading */
				otherClose:   "a.close",              /* Selector for alternate close buttons (e.g. "a.close") */
				beforeClose:  function(event){
					if ($gameContainer && $leaveContainer) {
						if ($gameFinishContainer.is(':hidden')) {
							$gameContainer.hide();
							$leaveContainer.show();
						} else {
							self.closeAppLayer();
						}
					} else if (self.getAppClassElement('featherlight-content').find('.js-app-error').length > 0) {
						self.closeAppLayer();
					}
					return false;
				},
		       /* Called before close. can return false to prevent opening of lightbox. Gets event as parameter, this contains all data */
				afterOpen:    function(){
					self.showLoadingInLayer();
					if (isRaw) {
						self.readContentAsyncRAW(readContentCallback);
					} else {
						self.readContentAsync(readContentCallback);
					}
					var $gameContainer = self.getAppClassElement('featherlight-content').find('.container');
					var	$leaveContainer = self.getAppClassElement('featherlight-content').find('.leave');
					$gameContainer.show();
					$leaveContainer.hide();
				},               /* Called after open. Gets event as parameter, this contains all data */
				afterContent: $.noop,                 /* Called after content is ready and has been set. Gets event as parameter, this contains all data */
				afterClose:   $.noop,                 /* Called after close. Gets event as parameter, this contains all data */
				onKeyDown:    function() {
				},                                   /* Called on key down for the frontmost featherlight */
				type:         'html',                   /* Specify content type. If unset, it will check for the targetAttrs value. */
				contentFilters: ['jquery', 'image', 'html', 'ajax', 'text'] /* List of content filters to use to determine the content */
				 /* Specify content type and data */
			});
		 },

		showFinishScreen: function(resetCallBack) {
			var self = this;
		 	var $innerDiv, $outerDiv;
		 	var $appContainer = getClass('container');
		 	$appContainer.hide();
		    $outerDiv = getClass('finishScreen');
		    $outerDiv.empty();
		    $innerDiv = $('<div>').addClass('finish');
		    $innerDiv.append($('<p>').text(gettext('Great!')));
		    var $play = $('<button/>',{'class' : 'play'}).text(gettext('Play Again'));
		    $play.click(function(){
		    	$outerDiv.hide();
		    	$appContainer.show();
		    	resetCallBack();
		    });
		    $innerDiv.append($play);
		    $outerDiv.append($innerDiv);
		    $outerDiv.show();
		},

	    /**
         * Read the current record asynchronously. The record is displayed when the call returns.
         */
        readRecordAsync: function () {
			$recordText = getClass('recordsText');
			$.ajax({
				type: 'GET',
				url: getReadRecordUrl(),
				dataType: 'xml',
				context: this,
				success: function (xml) {
				    var code, events, records, response = $(xml).find('response');
				    if (response.length > 0 && $(response[0]).attr('code')) {
				        code = $(response[0]).attr('code');
				        if (code === '200') {
				            events = $(xml).find('event');
				            if (events.length > 0) {
				                records = [];
				                events.each(function (i) { records[i] = parseInt($(this).text(), 10); });
				                currentRecord = records.max();
				                showCurrentRecord();
				            } else {
				                currentRecord = -1;
				                showRecordNotSetMessage();
				            }
				            displayRecordsDiv();
				        }
				    }
				}
			});
		},
        /**
         * Read a named configuration variable. Ignores site or global variables. For details see readNamedConfigVariableOnlyAccount
         */
        readNamedConfigVariableForAccount : function (variableName) {
            return readNamedConfigVariableOnlyAccount(variableName);
        },
        /**
         * Read a named configuration variable. For details see readNamedConfigVariable
         */
        readNamedConfigVariableAllLevels : function(variableName) {
            return readNamedConfigVariable(variableName);
        },

        /** check for drag and drop setting */
        checkDragAndDropSetting : function() {
            var accessibility = this.readNamedConfigVariableAllLevels('accessibility');
            accessibility = accessibility.replace(/\'/g,"\"");
            accessibility = accessibility.replace(/-/g,"\_");

            if( accessibility ){
                var parsed = $.parseJSON(accessibility);
                if( parsed ){
                    var dragAndDrop = parsed.drag_and_drop;
                    /* changed code for removing any chances of error , code more robust */
                    if( dragAndDrop !== undefined ){
                        if( dragAndDrop === 1 ){
                            return true;
                        }else if( dragAndDrop === 0 ){
                            return false;
                        }
                    }
                }
            }
            else {
                return true;
            }
        },

        /** Show Drag and Drop Warning */

        showDragAndDropWarning : function() {
            $gameContainer.hide();
            $dragAndDropSection.show();
        },

        playSound : function(url, app) {
        	if (soundManager) {
	        	soundManager.stopAll();
	            var currentSound;
	            if (soundManager.ok()) {
	                currentSound = soundManager.createSound({
	                        url: url,
	                        id: app,
	                        onload: function () {
	                            this.play();
	                        }
	                    }).load();
	            }
        	}
        },

        /**
         * Detect if whatever we are running on is touch-enabled.
         * source: http://modernizr.github.com/Modernizr/touch.html
         */
        is_touch_device: function () {
            var testA,testB,testC,testD,testE,testF,testG, try_catch

            var dragAndDropOverriden = overrideDragNDrop();
            if (dragAndDropOverriden) {
                return true;
            }

            try {
	            document.createEvent("TouchEvent");
	            try_catch = 1;
            } catch (e) {
	            try_catch = 0;
            }
            testA = (!!('ontouchstart' in window) ? 1 : 0);
            testB = try_catch;
            testC = (!!('createTouch' in document) ? 1 : 0);
            testD = (!!(typeof TouchEvent != "undefined") ? 1 : 0);
            testE = (!!(typeof Touch == "object") ? 1 : 0);
            testF = (!!('ontouchend' in document) ? 1 : 0);


            if (typeof NW != 'undefined') {
                testG = ( NW.Event.isSupported('touchstart') ? 1 : 0);
            } else {
                testG = 0;
            }
            /* Removing this since some of the modern browsers ( chrome/firefox ) recognize
            these events( can emulate touch devices ), and hence return false positives
            even when it is not a touch device , and users cannot be asked to change their
            settings. Also , we cannot recognize a Touch device as such , we can only check for
            Touch events. Hence , only testE returns absolutely correct result. */
            /*if( testA + testB + testC + testD + testE + testF + testG < 3 ){ //TODO: Adjust this number according to real world answers from various devices.
                return false;
            }*/
            if ( !testE ) {
                return false;
            } else {
				// since now drag and drop is supported in touch also, if touch and drag and drop is true , then actiavte tapntap false
				if (!dragAndDropOverriden) {
					return false;
				} else {
					return true;
				}
            }
        },

	    /**
         * Read assigment content asynchronously. An error screen is displayed if the content cannot be read.
         * @param {function} onReadContentSuccessCallback Function to call when the content has been successfully read, using the content text as argument.
         */
        readContentAsync: function (onReadContentSuccessCallback) {
		    var readContentAsyncSuccess = function (xml) {
		        var code, content, contentText, response = $(xml).find('response');
		        if (response.length === 0 || !$(response[0]).attr('code')) {
		            showErrorScreen(gettext('An error occurred when loading the task.'), 'The HTTP response was empty.');
		        } else {
		            code = $(response[0]).attr('code');
		            content = $(xml).find('content');
		            if (content.length < 1) {
		                showErrorScreen(gettext('An error occurred when loading the task.'), 'The response did not contain an XML element \'content\'.');
		            } else if (code === '200') {
		                parseContentLanguage($(content[0]));
		                contentText = $(content[0]).text();
		                contentText = contentText.replace(/\r\n/g, '\n');
		                contentText = contentText.replace(/ +/g, ' ');
		                contentText = contentText.replace(/ *\\ */g, '\\');
		                contentText = contentText.replace(/# /g, '#');
		                contentText = contentText.replace(/\* /g, '*');
		                contentText = contentText.replace(/\[ /g, '[');
		                contentText = contentText.replace(/ \]/g, ']');
		                contentText = contentText.replace(/[ \t]*\n[ \t]*/g, '\n');
		                contentText = contentText.trim();
		                try {
		                    onReadContentSuccessCallback(contentText);
		                } catch (e) {
		                    showContentErrorScreen(e.message);
		                }
		            } else if (code === '403') {
		                showErrorScreen(gettext('You are not authorized to view this task.'), 'HTTP status 403');
		            } else if (code === '404') {
		                showErrorScreen(gettext('The task does not exist.'), 'HTTP status 404');
		            } else {
		                showErrorScreen(gettext('An error occurred when loading the task.'), 'HTTP status ' + code);
		            }
		        }
		    };

		    $.ajax({
		        type: 'GET',
		        url: getContentUrl(),
		        dataType: 'xml',
		        context: this,
		        success: readContentAsyncSuccess,
		        error: function (jqXHR, textStatus, errorThrow) { showErrorScreen(gettext('An error occurred when loading the task.'), 'Error type: ' + textStatus); }
		    });
		},

        extractContent : function(xml) {
            var content = $(xml).find('content');
            parseContentLanguage($(content[0]));
            var contentText = $(content[0]).text();
		    contentText = contentText.replace(/\r\n/g, '\n');
		    contentText = contentText.replace(/ +/g, ' ');
		    contentText = contentText.replace(/ *\\ */g, '\\');
		    contentText = contentText.replace(/# /g, '#');
		    contentText = contentText.replace(/\* /g, '*');
		    contentText = contentText.replace(/\[ /g, '[');
		    contentText = contentText.replace(/ \]/g, ']');
		    contentText = contentText.replace(/[ \t]*\n[ \t]*/g, '\n');
		    contentText = contentText.trim();
            return contentText;
        },

		readContentAsyncRAW: function (onReadContentSuccessCallback) {
		    var readContentAsyncSuccess = function (xml) {
		        var code, content, contentText, response = $(xml).find('response');
		        if (response.length === 0 || !$(response[0]).attr('code')) {
		            showErrorScreen(gettext('An error occurred when loading the task.'), 'The HTTP response was empty.');
		        } else {
		            code = $(response[0]).attr('code');
		            content = $(xml).find('content');
		            if (code === '200') {
		                try {
		                    onReadContentSuccessCallback(xml);
		                } catch (e) {
		                    showContentErrorScreen(e.message);
		                }
		            } else if (code === '403') {
		                showErrorScreen(gettext('You are not authorized to view this task.'), 'HTTP status 403');
		            } else if (code === '404') {
		                showErrorScreen(gettext('The task does not exist.'), 'HTTP status 404');
		            } else {
		                showErrorScreen(gettext('An error occurred when loading the task.'), 'HTTP status ' + code);
		            }
		        }
		    };

		    $.ajax({
		        type: 'GET',
		        url: getContentUrl(),
		        dataType: 'xml',
		        context: this,
		        success: readContentAsyncSuccess,
		        error: function (jqXHR, textStatus, errorThrow) { showErrorScreen(gettext('An error occurred when loading the task.'), 'Error type: ' + textStatus); }
		    });
		}
	};
};