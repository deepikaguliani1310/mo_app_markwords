// Defines a function 'shuffle' which allows any type of array to be shuffled.
if (typeof Array.prototype.shuffle !== 'function') {
	Array.prototype.shuffle = function () {
		var s = [];
		while (this.length) {
			s.push(this.splice(Math.random() * this.length, 1)[0]);
		}
		while (s.length) {
			this.push(s.pop());
		}
		return this;
	};
}

// Defines a function 'max' which returns the maximum value in an array.
if (typeof Array.prototype.max !== 'function') {
	Array.prototype.max = function () {
		var i, max = this[0];
		for (i = 1; this[i]; i += 1) {
			if (this[i] > max) {
				max = this[i];
			}
		}
		return max;
	};
}

// Defines 'trim' for older versions of IE.
if (typeof String.prototype.trim !== 'function') {
	String.prototype.trim = function () {
		return this.replace(/^\s+|\s+$/g, '');
	};
}

// Defines 'indexOf' for older versions of IE.
if (typeof Array.prototype.indexOf !== 'function') {
	Array.prototype.indexOf = function (find, i) {
		var n;
		if (i === undefined) { i = 0; }
		if (i < 0) { i += this.length; }
		if (i < 0) { i = 0; }
		for (n = this.length; i < n; i += 1) {
			if (typeof (this.i) !== undefined && this[i] === find) { return i; }
		}
		return -1;
	};
}

// Defines 'lastIndexOf' for older versions of IE.
if (typeof Array.prototype.lastIndexOf !== 'function') {
	Array.prototype.lastIndexOf = function (find, i) {
		if (i === undefined) { i = this.length - 1; }
		if (i < 0) { i += this.length; }
		if (i > this.length - 1) { i = this.length - 1; }
		i += 1; /* i++ because from-argument is sadly inclusive */
		for (i; i > 0; i -= 1) {
			if (typeof (this.i) !== undefined && this[i] === find) { return i; }
		}
		return -1;
	};
}

// Defines a function 'occurrences' which counts occurrences of a particular substring in a string.
if (typeof String.prototype.occurrences !== 'function') {
	String.prototype.occurrences = function (searchValue) {
		var n = 0, pos = 0;
		while (true) {
			pos = this.indexOf(searchValue, pos);
			if (pos !== -1) {
				n += 1;
				pos += searchValue.length;
			} else {
				break;
			}
		}
		return (n);
	};
}

// Defines a function 'contains' which determines if a particular substring is present in a string.
if (typeof String.prototype.contains !== 'function') {
	String.prototype.contains = function (searchValue) {
		return this.indexOf(searchValue) > -1;
	};
}

$.fn.hasScrollBar = function () {
    var _elm = $(this)[0];
    var _hasScrollBar = false;
    if ((_elm.clientHeight < _elm.scrollHeight) || (_elm.clientWidth < _elm.scrollWidth)) {
        _hasScrollBar = true;
    }
    return _hasScrollBar;
}