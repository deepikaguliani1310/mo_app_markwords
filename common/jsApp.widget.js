/*jslint indent:4, maxerr:50, white:true, browser:true, vars: true, sloppy: true, nomen: true */
/*global jQuery, $ */
(function($) {
	'use strict';

	   $.widget('portfolio.jsApp', {
        /* default options */
        options : {
            'layout'      : 'common',
            'app_name'    : '',
            'prefix'      : ''
        },

        /* create */
        _create : function() {
            if (this.options.layout === 'kids') {
                this._show_start_screen();
            } else {
                 this._show_loading();
            }
        },

        _getContentUrl : function() {

        },
        
        
        _readContent : function() {
            
        },

        _init : function() {
            // for reset/set things and app specfic stuff
        },

        /* set of utility functions */
        _load_app_html : function() {

        },

        _gettext : function(str) {
            /* Client language not set */
            if ( ! this.options.ui_language ) {
                return str;
            }
            /* Lookup table must exist */
            if ( ! this.messages ) {
                return str;
            }
            /* Original string (msgid) must exist */
            if ( ! this.messages[str] ) {
                return str;
            }
            /* Translation must exist */
            if ( ! this.messages[str][this.options.ui_language] ) {
                return str;
            }
            /* Everything is ok, return translated string */
            return this.messages[str][this.options.ui_language];
        },

        _on_messages_loaded : function() {

        },

        _apply_i18n : function() {

        },

        /* callback executed after the app HTML has been injected into the element*/
        _on_app_html_loaded : function(response, status, jqxhr) {
            this._apply_i18n();
        },

        /* end set of utility functions */
        
        _show_loading : function() {
            
        },
        
        _show_start_screen : function() {
            var self = this;
            this._container = $('<div/>').addClass('js-app-container').attr('id','js-app-'+ this.options.app_name);
            console.log('this.appname', this.options.app_name);
        },
        
        read_content_async : function() {
            try {
                
                
            } catch(e) {
                
            }
        },

        prepare_content : function() {

        },
        
        read_content_async_raw : function(onReadContentSuccessCallback) {
            var self = this;
            var readContentAsyncSuccess = function (xml) {
                var code, content, contentText, response = $(xml).find('response');
                if (response.length === 0 || !$(response[0]).attr('code')) {
                    showErrorScreen(gettext('An error occurred when loading the task.'), 'The HTTP response was empty.');
                } else {
                    code = $(response[0]).attr('code');
                    content = $(xml).find('content');
                    if (code === '200') {
                        try {
                            onReadContentSuccessCallback(xml);
                        } catch (e) {
                            showContentErrorScreen(e.message);
                        }
                    } else if (code === '403') {
                        showErrorScreen(gettext('You are not authorized to view this task.'), 'HTTP status 403');
                    } else if (code === '404') {
                        showErrorScreen(gettext('The task does not exist.'), 'HTTP status 404');
                    } else {
                        showErrorScreen(gettext('An error occurred when loading the task.'), 'HTTP status ' + code);
                    }
                }
            };

            $.ajax({
                type: 'GET',
                url: self._getContentUrl(),
                dataType: 'xml',
                context: this,
                success: readContentAsyncSuccess,
                error: function (jqXHR, textStatus, errorThrow) { self._show_error_screen(self._gettext('An error occurred when loading the task.'), 'Error type: ' + textStatus); }
            });
        },

        /* destroy */
        _destroy : function() {

        }

	});
}(jQuery));