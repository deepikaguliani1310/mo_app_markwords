#!/usr/bin/env node
var express = require("express")
  , path    = require('path');

var app = express();
var config = { port : "1111" , host : "127.0.0.1" };

app.use(express.static(__dirname));

app.listen( config.port, config.host );
console.log("Server at http://" + config.host + ':' + config.port.toString() + '/');
